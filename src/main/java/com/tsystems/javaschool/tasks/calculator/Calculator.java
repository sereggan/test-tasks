package com.tsystems.javaschool.tasks.calculator;

import java.text.DecimalFormat;
import java.util.ArrayDeque;
import java.util.Deque;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        // TODO: Implement the logic here
        if(statement==null||statement.isEmpty()) return null; // Проверка на null
        if(!checkInput(statement)) return null;
        DecimalFormat decimalFormat=new DecimalFormat("#.####"); // чтобы избежать лишнего .0
        Double result = calculateRPN(expressionToRPN(statement));
        if(result.isInfinite()) return null; // При делении на 0
        return decimalFormat.format(result).replaceAll(",",".");
    }

    private String expressionToRPN(String expression){
        String current = "";
        Deque<Character> stack = new ArrayDeque<>();

        int priority;
        for(int i=0;i<expression.length();i++){
            priority = getPriority(expression.charAt(i));
            if(priority==0) current+=expression.charAt(i); // Если не операция, то просто прибавляем
            if(priority==1) stack.push(expression.charAt(i)); // Если (, то просто кладем в стек

            if(priority>1){
                current+=' ';
                while (!stack.isEmpty()){
                    if(getPriority(stack.peek())>=priority) current+=stack.pop(); // Пока не попадется с приоритетом меньше текущего, вытаскиваем значения из стека
                    else break;
                }
                stack.push(expression.charAt(i));
            }
            if(priority==-1){
                current+=' ';
                while (getPriority(stack.peek())!=1) current+=stack.pop(); // Пока не найдем закрывающую скобку вытаскиваем значения из стека
                stack.pop();
            }
        }
        while(!stack.isEmpty()) current+= stack.pop(); //  Оставшиеся значения кладем в нашу строку
        return current;
    }

    private double calculateRPN(String expression) {
        String operand = new String();
        Deque<Double> stack = new ArrayDeque<>();
        for(int i=0;i<expression.length();i++){
            if(expression.charAt(i)==' ')continue;
            if(getPriority(expression.charAt(i))==0){   //Если не операция, то вытаскиваем число до пробела или операции
                while(expression.charAt(i)!=' '&&getPriority(expression.charAt(i))==0){
                    operand+=expression.charAt(i++);
                    if(i==expression.length()) break;
                }
                stack.push(Double.parseDouble(operand));
                operand=new String();
            }
            if(getPriority(expression.charAt(i))>1){ // не скобка и не число
                double firstNumber = stack.pop();  // Вытаскиваем 2 числа, проводим операцию и кладем назад в стек
                double secondNumber = stack.pop();
                if(expression.charAt(i)=='+')stack.push(firstNumber+secondNumber);
                if(expression.charAt(i)=='-')stack.push(secondNumber-firstNumber);
                if(expression.charAt(i)=='*')stack.push(secondNumber*firstNumber);
                if(expression.charAt(i)=='/')stack.push(secondNumber/firstNumber);

            }
        }
        return stack.pop();
    }

    private int getPriority(char token){
        if(token=='*' || token=='/') return 3;
        else  if (token=='+' || token=='-') return 2;
        else if(token=='(') return 1;
        else if(token==')') return -1;
        else return 0;
    }

    private boolean checkInput(String expression){
                int leftBracket = 0;
                int rightBracket = 0;
                for(int i=0;i<expression.length();i++){
                    char currentChar = expression.charAt(i);
                    if(currentChar==',') return false; // Проверка на ,
                    if(currentChar=='(') leftBracket+=1;
                    if(currentChar==')') rightBracket+=1;
                    if(leftBracket<rightBracket) return false;
                    if(getPriority(currentChar)!=0||expression.charAt(i)=='.'){
                        if(i!=expression.length()-1){
                            if(currentChar==expression.charAt(i+1)) return false;
                        }
                    }
                }
        return leftBracket == rightBracket;
    }
}
