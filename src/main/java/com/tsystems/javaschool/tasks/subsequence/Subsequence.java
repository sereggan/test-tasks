package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        // TODO: Implement the logic here
        if (x==null||y==null) throw new IllegalArgumentException(); // проверка на null
        if (x.isEmpty()&&y.isEmpty()) return true; // проверка на 2 пустых массива
        if(x.size()>y.size()) return false; //  если 1 лист меньше 2 листа, то априори нельзя
        if(x.size()==0) return true;
        int i=0;
        int j = 0;
        while (j<y.size()){
            if(x.get(i)==y.get(j)){  // если элементы равны то сравниваем дальше
                i+=1;
                j+=1;
            }else j+=1; // либо же идем дальше по 2 списку
            if(i == x.size()) return true; // если все элементы 1 вошли в 2
        }
        return false; // если не нашли подпоследовательность
    }
}
