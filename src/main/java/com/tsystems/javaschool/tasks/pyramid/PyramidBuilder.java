package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;


public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        // TODO : Implement your solution here
        if(!isCorrectArray(inputNumbers.size())){ // Проверка на возможность построить пирамиду из массива данной длины
            throw new CannotBuildPyramidException();
        }

        try {
            Collections.sort(inputNumbers);  // Про сортировку в задании ничего не сказано
        }catch (NullPointerException | OutOfMemoryError e){  //Если в листе есть null
            throw  new CannotBuildPyramidException();
        }

        int height = getHeightOfArray(inputNumbers.size());
        int width = height*2 - 1;
        int[][] array;

        try {
            array = new int[height][width]; // max размер массива Integer.MAX_VALUE - 2
        }catch (NegativeArraySizeException e){
            throw  new CannotBuildPyramidException();
        }

        int mid = width/2;
        int counter = 0;
        boolean isGoodNumber = true;
        for(int i=0;i<array.length;i++){
            for(int j =0;j<array[i].length;j++){
                if((j<=(mid+i)&&j>=(mid-i))){   //Ограничил до области, где могут быть не нули
                    if(isGoodNumber) {
                        array[i][j] = inputNumbers.get(counter);
                        counter += 1;
                    }
                    isGoodNumber = !isGoodNumber;  // Для чередования
                }else {
                    array[i][j] =0;
                }
            }
            isGoodNumber = true;
        }

        return array;
    }

    private boolean isCorrectArray(int num){  // Проверка, можно ли из такого массива построить пирамиду
        if (num < 0)
            return false;
        int sum = 0;

        for (int n = 1; sum <= num; n++)
        {
            sum = sum + n;
            if (sum == num)
                return true;
        }
        return false;
    }

    private int getHeightOfArray(int n){  // Получаем высоту нашей пирамиды
        int sum = 0;

        for (int i = 1; i <= n; i++)
        {
            sum = sum + i;
            if (sum == n)
                return i;
        }
        return -1;
    }
}
